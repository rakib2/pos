namespace POSInventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductTableColumnModify : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Product", "DrugGenericNameID");
            DropColumn("dbo.Product", "ManufacturerID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Product", "ManufacturerID", c => c.Int(nullable: false));
            AddColumn("dbo.Product", "DrugGenericNameID", c => c.Int(nullable: false));
        }
    }
}
