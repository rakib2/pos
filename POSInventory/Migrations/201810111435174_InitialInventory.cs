namespace POSInventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialInventory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                        Description = c.String(),
                        isActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        DrugGenericNameID = c.Int(nullable: false),
                        ManufacturerID = c.Int(nullable: false),
                        CategoryID = c.Int(nullable: false),
                        AlertQty = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Category", t => t.CategoryID, cascadeDelete: true)
                .Index(t => t.CategoryID);
            
            CreateTable(
                "dbo.PurchaseItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PurchaseID = c.String(),
                        ProductID = c.Int(nullable: false),
                        Qty = c.Int(nullable: false),
                        CostPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SellPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Expiry = c.DateTime(nullable: false),
                        Purchase_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.ProductID, cascadeDelete: true)
                .ForeignKey("dbo.Purchase", t => t.Purchase_Id)
                .Index(t => t.ProductID)
                .Index(t => t.Purchase_Id);
            
            CreateTable(
                "dbo.Purchase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceNo = c.String(),
                        Date = c.DateTime(nullable: false),
                        SupplierID = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(precision: 18, scale: 2),
                        Tax = c.Decimal(precision: 18, scale: 2),
                        GrandTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsPaid = c.Boolean(nullable: false),
                        LastUpdated = c.DateTime(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Supplier", t => t.SupplierID, cascadeDelete: true)
                .Index(t => t.SupplierID);
            
            CreateTable(
                "dbo.Supplier",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Address = c.String(),
                        Contact = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Stock",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductID = c.Int(nullable: false),
                        InitialQty = c.Int(nullable: false),
                        Qty = c.Int(nullable: false),
                        CostPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SellPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExpiryDate = c.DateTime(nullable: false),
                        ItemExpired = c.Boolean(nullable: false),
                        Stop_Notification = c.Boolean(nullable: false),
                        PurchaseID = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.Notification",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LowStock = c.Int(nullable: false),
                        ToExpire = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Sales",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Tax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GrandTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UserID = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SalesItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StockID = c.Int(nullable: false),
                        SalesID = c.Int(nullable: false),
                        Qty = c.Int(nullable: false),
                        Rate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sales", t => t.SalesID, cascadeDelete: true)
                .ForeignKey("dbo.Stock", t => t.StockID, cascadeDelete: true)
                .Index(t => t.StockID)
                .Index(t => t.SalesID);
            
            CreateTable(
                "dbo.SalesReturn",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalesID = c.Int(nullable: false),
                        Subtotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NetTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(),
                        ReturnedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sales", t => t.SalesID, cascadeDelete: true)
                .Index(t => t.SalesID);
            
            CreateTable(
                "dbo.SalesReturnDetail",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalesReturnID = c.Int(nullable: false),
                        StockID = c.Int(nullable: false),
                        Qty = c.Int(nullable: false),
                        Rate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SalesReturn", t => t.SalesReturnID, cascadeDelete: true)
                .Index(t => t.SalesReturnID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SalesReturnDetail", "SalesReturnID", "dbo.SalesReturn");
            DropForeignKey("dbo.SalesReturn", "SalesID", "dbo.Sales");
            DropForeignKey("dbo.SalesItem", "StockID", "dbo.Stock");
            DropForeignKey("dbo.SalesItem", "SalesID", "dbo.Sales");
            DropForeignKey("dbo.Stock", "ProductID", "dbo.Product");
            DropForeignKey("dbo.Purchase", "SupplierID", "dbo.Supplier");
            DropForeignKey("dbo.PurchaseItem", "Purchase_Id", "dbo.Purchase");
            DropForeignKey("dbo.PurchaseItem", "ProductID", "dbo.Product");
            DropForeignKey("dbo.Product", "CategoryID", "dbo.Category");
            DropIndex("dbo.SalesReturnDetail", new[] { "SalesReturnID" });
            DropIndex("dbo.SalesReturn", new[] { "SalesID" });
            DropIndex("dbo.SalesItem", new[] { "SalesID" });
            DropIndex("dbo.SalesItem", new[] { "StockID" });
            DropIndex("dbo.Stock", new[] { "ProductID" });
            DropIndex("dbo.Purchase", new[] { "SupplierID" });
            DropIndex("dbo.PurchaseItem", new[] { "Purchase_Id" });
            DropIndex("dbo.PurchaseItem", new[] { "ProductID" });
            DropIndex("dbo.Product", new[] { "CategoryID" });
            DropTable("dbo.SalesReturnDetail");
            DropTable("dbo.SalesReturn");
            DropTable("dbo.SalesItem");
            DropTable("dbo.Sales");
            DropTable("dbo.Notification");
            DropTable("dbo.Stock");
            DropTable("dbo.Supplier");
            DropTable("dbo.Purchase");
            DropTable("dbo.PurchaseItem");
            DropTable("dbo.Product");
            DropTable("dbo.Category");
        }
    }
}
