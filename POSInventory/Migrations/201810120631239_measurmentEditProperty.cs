namespace POSInventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class measurmentEditProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UOM", "Munit", c => c.String(nullable: false));
            DropColumn("dbo.UOM", "Uom");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UOM", "Uom", c => c.String(nullable: false));
            DropColumn("dbo.UOM", "Munit");
        }
    }
}
