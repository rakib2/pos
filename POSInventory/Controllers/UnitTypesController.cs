﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using POSInventory.Models;

namespace POSInventory.Controllers
{
    public class UnitTypesController : Controller
    {
        private InventoryContext db = new InventoryContext();

        // GET: UnitTypes
        public ActionResult Index()
        {
            return View(db.Units.ToList());
        }

        // GET: UnitTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UOM uOM = db.Units.Find(id);
            if (uOM == null)
            {
                return HttpNotFound();
            }
            return View(uOM);
        }

        // GET: UnitTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UnitTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Munit,Description")] UOM uOM)
        {
            if (ModelState.IsValid)
            {
                db.Units.Add(uOM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(uOM);
        }

        // GET: UnitTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UOM uOM = db.Units.Find(id);
            if (uOM == null)
            {
                return HttpNotFound();
            }
            return View(uOM);
        }

        // POST: UnitTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Munit,Description")] UOM uOM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uOM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(uOM);
        }

        // GET: UnitTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UOM uOM = db.Units.Find(id);
            if (uOM == null)
            {
                return HttpNotFound();
            }
            return View(uOM);
        }

        // POST: UnitTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UOM uOM = db.Units.Find(id);
            db.Units.Remove(uOM);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
