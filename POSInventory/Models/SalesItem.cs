﻿using System.ComponentModel.DataAnnotations;

namespace POSInventory.Models
{
    public class SalesItem
    {
        public int Id { get; set; }
        public int StockID { get; set; }
        public int SalesID { get; set; }

        [Required]
        public int Qty { get; set; }

        [Required]
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }

        public virtual Stock Stock { get; set; }
        public virtual Sales Sales { get; set; }
    }
}