﻿using System;
using System.ComponentModel.DataAnnotations;

namespace POSInventory.Models
{
    public class Stock
    {
        public int Id { get; set; }
        [Required]
        public int ProductID { get; set; }

        [Range(0, 9999999)]
        public int InitialQty { get; set; }

        [Required]
        [Range(0, 100000)]
        public int Qty { get; set; }

        [Required]
        [Range(0, 1000000, ErrorMessage = "Out of range!")]
        public decimal CostPrice { get; set; }

        [Required]
        [Range(0, 1000000, ErrorMessage = "Out of range!")]
        public decimal SellPrice { get; set; }
        

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ExpiryDate { get; set; }

        public bool ItemExpired { get; set; }

        public bool Stop_Notification { get; set; }

        public string PurchaseID { get; set; }


        //references
        public virtual Product Product { get; set; }
    }
}