﻿using System;
using System.ComponentModel.DataAnnotations;

namespace POSInventory.Models
{
    public class PurchaseItem
    {
        public int Id { get; set; }
        public string PurchaseID { get; set; }
        public int ProductID { get; set; }

        [Range(0, 1000000, ErrorMessage = "Not an acceptable Quantity!")]
        public int Qty { get; set; }

        [Range(0.00, 99999999.99)]
        public decimal CostPrice { get; set; }
        public decimal SellPrice { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Expiry { get; set; }

        public virtual Product Item { get; set; }
        public virtual Purchase Purchase { get; set; }
    }
}