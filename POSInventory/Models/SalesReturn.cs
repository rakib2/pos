﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace POSInventory.Models
{
    public class SalesReturn
    {
        public int Id { get; set; }
        public int SalesID { get; set; }
        public decimal Subtotal { get; set; }
        public decimal Discount { get; set; }
        public decimal NetTotal { get; set; }
        public string Description { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ReturnedDate { get; set; }

        public virtual ICollection<SalesReturnDetail> SalesReturnDetails { get; set; }
        public virtual Sales Sales { get; set; }
    }
}