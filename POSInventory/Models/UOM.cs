﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSInventory.Models
{
    public class UOM
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Measurment unit is required.")]
        [Display(Name = "Measurment unit")]
        public string Munit { get; set; }

        public string Description { get; set; }
    }
}