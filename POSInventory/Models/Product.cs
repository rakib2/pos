﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace POSInventory.Models
{
    public class Product
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Product Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Categeory")]
        public int CategoryID { get; set; }

        [Display(Name = "Alert Quantity")]
        public int AlertQty { get; set; }

        public string Description { get; set; }

        public Category Category { get; set; }
        public virtual ICollection<Stock> Stocks { get; set; }
        public virtual ICollection<PurchaseItem> PurchaseItems { get; set; }
    }
}