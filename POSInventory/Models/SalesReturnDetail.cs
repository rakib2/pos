﻿namespace POSInventory.Models
{
    public class SalesReturnDetail
    {
        public int Id { get; set; }
        public int SalesReturnID { get; set; }
        public int StockID { get; set; }
        public int Qty { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }

        public virtual SalesReturn SalesReturn { get; set; }
    }
}